module gitee.com/shijun123456/kcgin

go 1.12

require (
	github.com/OwnLocal/goes v1.0.0
	github.com/Unknwon/goconfig v0.0.0-20191126170842-860a72fb44fd
	github.com/beego/goyaml2 v0.0.0-20130207012346-5545475820dd
	github.com/beego/x2j v0.0.0-20131220205130-a0352aadc542
	github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gogo/protobuf v1.3.1
	github.com/lib/pq v1.1.1
	github.com/mattn/go-sqlite3 v2.0.1+incompatible
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/wendal/errors v0.0.0-20130201093226-f66c77a7882b // indirect
)
